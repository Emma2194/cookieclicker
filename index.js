document.addEventListener("DOMContentLoaded", function () {
  var isTest = false; // Cambia a false para desactivar el modo de prueba
  var clickCounter = isTest ? 500000000000000 : 0;
  var cursorCount = 0;
  var superCursorCount = 0;
  var hyperCursorCount = 0;
  var cursorInitialCost = 10;
  var superCursorInitialCost = 100;
  var hyperCursorInitialCost = 1000;
  var maxCursors = 50; // Límite de 50 cursores
  var maxSuperCursors = 50; // Límite de 10 Super-Cursores
  var maxHyperCursors = 50; // Límite de 10 Super-Cursores
  var cursorRotation = 270; // Rotación inicial
  var superCursorRotation = 270; // Rotación inicial
  var hyperCursorRotation = 270; // Rotación inicial
  var cursorCostMultiplier = 1.1; // Factor de costo para nuevos cursores
  var superCursorCostMultiplier = 1.1; // Factor de costo para nuevos Super-Cursores
  var hyperCursorCostMultiplier = 1.1; // Factor de costo para nuevos hyper-Cursores
  var superCursorRate = 50; // Cantidad de clics por segundo generados por cada Super-Cursor
  var hyperCursorRate = 100; // Cantidad de clics por segundo generados por cada Super-Cursor
  var superCursorInterval; // Intervalo para generar clics por segundo
  var hyperCursorInterval; // Intervalo para generar clics por segundo

  var cookieImg = document.querySelector(".cookie-img");
  var clickCounterElement = document.getElementById("click-counter");
  var cursorRing = document.getElementById("cursor-ring");
  var superCursorRing = document.getElementById("super-cursor-ring");
  var hyperCursorRing = document.getElementById("hyper-cursor-ring");
  var buyCursorButton = document.getElementById("buy-cursor");
  var buySuperCursorButton = document.getElementById("buy-super-cursor");
  var buyHyperCursorButton = document.getElementById("buy-hyper-cursor");
  var cElement = document.getElementById("cursores-actuales"); // Elemento para mostrar clics por segundo
  var scElement = document.getElementById("super-cursores-actuales"); // Elemento para mostrar clics por segundo
  var hcElement = document.getElementById("hyper-cursores-actuales"); // Elemento para mostrar clics por segundo

  if (isTest) {
    updateClickCounter(); // Actualiza el contador inicial en el modo de prueba
  }

  cookieImg.addEventListener("click", function () {
    // Incrementa el contador de clics
    clickCounter++;

    // Aplica la animación de reducción de tamaño y restauración
    cookieImg.classList.add("shrink");
    setTimeout(function () {
      cookieImg.classList.remove("shrink");
    }, 200); // Ajusta la duración de la animación según sea necesario

    // Muestra el contador de clics en el HTML con formato
    updateClickCounter();
  });

  buyCursorButton.addEventListener("click", function () {
    // Calcula el costo del próximo cursor
    var cursorCost = Math.floor(
      cursorInitialCost * Math.pow(cursorCostMultiplier, cursorCount)
    );

    // Verifica si hay suficientes clics para comprar un cursor y no excede el límite
    if (clickCounter >= cursorCost && cursorCount < maxCursors) {
      // Resta el costo del contador
      clickCounter -= cursorCost;

      // Incrementa el contador de cursores
      cursorCount++;

      // Ajusta el giro del cursor propio
      cursorRotation += 7.2;

      // Muestra el contador de clics y cursores en el HTML con formato
      updateClickCounter();
      updateCursorCount();

      // Actualiza el texto del botón con el costo del próximo cursor
      buyCursorButton.innerText =
        "Comprar cursor (" +
        formatNumber(
          Math.floor(cursorInitialCost * Math.pow(cursorCostMultiplier, cursorCount))
        ) +
        " galletas)";

      // Calcula las coordenadas x e y del cursor en el círculo
      var radius = 150; // Radio para que el diámetro sea de 300px
      var angle = (2 * Math.PI * cursorCount) / maxCursors;
      var x = Math.cos(angle) * radius + 150; // Ajusta la posición x según el centro de la galleta
      var y = Math.sin(angle) * radius + 150; // Ajusta la posición y según el centro de la galleta

      // Crea un nuevo elemento para representar el cursor en el anillo
      var cursor = document.createElement("div");
      cursor.className = "cursor";
      cursorRing.appendChild(cursor);

      // Aplica las coordenadas calculadas al estilo del cursor
      cursor.style.left = x + "px";
      cursor.style.top = y + "px";

      // Aplica la rotación del cursor propio
      cursor.style.transform =
        "rotate(" + cursorRotation + "deg) translateY(-50%)";
      console.log(cursor.style.transform);

      // Inicia el temporizador para agregar 1 al contador por cada .5 segundos
      setInterval(function () {
        clickCounter += cursorCount;
        updateClickCounter();
        updateSuperCursorInterval(); // Agrega esta línea para actualizar el intervalo del Super-Cursor
      }, 500);
    } else {
      alert("No puedes comprar más cursores o no tienes suficientes galletas.");
    }
  });

  buySuperCursorButton.addEventListener("click", function () {
    // Calcula el costo del próximo Super-Cursor
    var superCursorCost = Math.floor(
      superCursorInitialCost * Math.pow(superCursorCostMultiplier, superCursorCount)
    );

    // Verifica si hay suficientes clics para comprar un Super-Cursor y no excede el límite
    if (clickCounter >= superCursorCost && superCursorCount < maxSuperCursors) {
      // Resta el costo del contador
      clickCounter -= superCursorCost;

      // Incrementa el contador de Super-Cursores
      superCursorCount++;

      // Muestra el contador de clics y Super-Cursores en el HTML con formato
      updateClickCounter();
      updateSuperCursorCount();

      // Actualiza el texto del botón con el costo del próximo Super-Cursor
      buySuperCursorButton.innerText =
        "Comprar Super-Cursor (" +
        formatNumber(
          Math.floor(
            superCursorInitialCost * Math.pow(superCursorCostMultiplier, superCursorCount)
          )
        ) +
        " galletas)";
      // Ajusta el giro del cursor propio
      superCursorRotation += 7.2;
      // Calcula las coordenadas x e y del cursor en el círculo
      var radius = 170; // Radio para que el diámetro sea de 300px
      var angle = (2 * Math.PI * superCursorCount) / maxSuperCursors;
      var x = Math.cos(angle) * radius + 200; // Ajusta la posición x según el centro de la galleta
      var y = Math.sin(angle) * radius + 200; // Ajusta la posición y según el centro de la galleta

      // Crea un nuevo elemento para representar el cursor en el anillo
      var cursor = document.createElement("div");
      cursor.className = "super-cursor";
      superCursorRing.appendChild(cursor);

      // Aplica las coordenadas calculadas al estilo del cursor
      cursor.style.left = x + "px";
      cursor.style.top = y + "px";

      // Aplica la rotación del cursor propio
      cursor.style.transform =
        "rotate(" + superCursorRotation + "deg) translateY(-50%)";

      // Inicia el intervalo solo si no está en ejecución
      if (!superCursorInterval) {
        superCursorInterval = setInterval(function () {
          clickCounter += superCursorRate * superCursorCount; // Ajuste para multiplicar por la cantidad de Super-Cursores
          updateClickCounter();
        }, 1000);
      }
    } else {
      alert(
        "No puedes comprar más Super-Cursores o no tienes suficientes galletas."
      );
    }
  });

  buyHyperCursorButton.addEventListener("click", function () {
    // Calcula el costo del próximo Super-Cursor
    var hyperCursorCost = Math.floor(
      hyperCursorInitialCost * Math.pow(hyperCursorCostMultiplier, hyperCursorCount)
    );

    // Verifica si hay suficientes clics para comprar un Hyper-Cursor y no excede el límite
    if (clickCounter >= hyperCursorCost && hyperCursorCount < maxHyperCursors) {
      // Resta el costo del contador
      clickCounter -= hyperCursorCost;

      // Incrementa el contador de Hyper-Cursores
      hyperCursorCount++;

      // Muestra el contador de clics y Hyper-Cursores en el HTML con formato
      updateClickCounter();
      updateHyperCursorCount();

      // Actualiza el texto del botón con el costo del próximo Hyper-Cursor
      buyHyperCursorButton.innerText =
        "Comprar Hyper-Cursor (" +
        formatNumber(
          Math.floor(
            hyperCursorInitialCost * Math.pow(hyperCursorCostMultiplier, hyperCursorCount)
          )
        ) +
        " galletas)";

      // Ajusta el giro del cursor propio
      hyperCursorRotation += 7.2;
      // Calcula las coordenadas x e y del cursor en el círculo
      var radius = 190; // Radio para que el diámetro sea de 300px
      var angle = (2 * Math.PI * hyperCursorCount) / maxHyperCursors;
      var x = Math.cos(angle) * radius + 300; // Ajusta la posición x según el centro de la galleta
      var y = Math.sin(angle) * radius + 300; // Ajusta la posición y según el centro de la galleta

      // Crea un nuevo elemento para representar el cursor en el anillo
      var cursor = document.createElement("div");
      cursor.className = "hyper-cursor";
      hyperCursorRing.appendChild(cursor);

      // Aplica las coordenadas calculadas al estilo del cursor
      cursor.style.left = x + "px";
      cursor.style.top = y + "px";

      // Aplica la rotación del cursor propio
      cursor.style.transform =
        "rotate(" + hyperCursorRotation + "deg) translateY(-50%)";

      // Inicia el intervalo solo si no está en ejecución
      if (!hyperCursorInterval) {
        hyperCursorInterval = setInterval(function () {
          clickCounter += hyperCursorRate * hyperCursorCount; // Ajuste para multiplicar por la cantidad de hyper-Cursores
          updateClickCounter();
        }, 1000);
      }
    } else {
      alert(
        "No puedes comprar más Hyper-Cursores o no tienes suficientes galletas."
      );
    }
  });

  // Función para actualizar el intervalo del Super-Cursor
  function updateSuperCursorInterval() {
    clearInterval(superCursorInterval);
    superCursorInterval = setInterval(function () {
      clickCounter += superCursorRate * superCursorCount;
      updateClickCounter();
    }, 1000);
  }

  function updateClickCounter() {
    clickCounterElement.innerText = formatNumber(clickCounter); // Formato de separación por miles
  }

  function updateCursorCount() {
    // Puedes mostrar la cantidad de cursores en algún lugar de tu interfaz si lo deseas
    // Por ejemplo: document.getElementById('cursor-count').innerText = cursorCount;
    cElement.innerText = "Cursores: " + cursorCount;
  }

  function updateSuperCursorCount() {
    // Puedes mostrar la cantidad de Super-Cursores en algún lugar de tu interfaz si lo deseas
    // Por ejemplo: document.getElementById('super-cursor-count').innerText = superCursorCount;

    // Muestra la cantidad de clics por segundo generados por los Super-Cursores
    scElement.innerText = "Super Cursores: " + superCursorCount;
  }

  function updateHyperCursorCount() {
    // Puedes mostrar la cantidad de Super-Cursores en algún lugar de tu interfaz si lo deseas
    // Por ejemplo: document.getElementById('super-cursor-count').innerText = superCursorCount;

    // Muestra la cantidad de clics por segundo generados por los Super-Cursores
    hcElement.innerText = "Hyper Cursores: " + hyperCursorCount;
  }
  // Función para formatear números con separación por miles
  function formatNumber(number) {
    return number.toLocaleString();
  }
});
